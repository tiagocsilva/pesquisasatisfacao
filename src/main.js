// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueResource from 'vue-resource'

import '../node_modules/bootstrap-css-only/css/bootstrap.min.css'
import '../node_modules/font-awesome/css/font-awesome.min.css'
import './assets/awesome-bootstrap-checkbox.css'

Vue.config.productionTip = false
Vue.use(VueResource);

/* eslint-disable no-new */
var vm = new Vue({
  el: '#app',
  template: '<App/>',
  components: { App }
});


